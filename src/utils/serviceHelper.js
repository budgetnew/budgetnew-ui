import axios from 'axios';
import store from '../redux/store';
import { logoutSagaAC } from '../redux/auth';
import config from '../../config';


axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
axios.defaults.withCredentials = true;

const port = window.location.protocol === 'https:' ? 3001 : 3000;
let API_ROOT = `${window.location.protocol}//${window.location.hostname}:${port}/api/v1`;
if (config.API_ROOT_URL) {
  API_ROOT = `${config.API_ROOT_URL}/api/v1`;
}

const checkAuth = (response) => {
  if (response.status === 401) {
    store.dispatch(logoutSagaAC());
  } else {
    return response;
  }
};

const sendRequest = ({ serviceFunc, url, payload }) => new Promise((resolve, reject) => {
  serviceFunc(`${API_ROOT}${url}`, payload)
    .then(checkAuth)
    .then((response) => {
      if (response.status >= 400) {
        return reject({ response });
      }
      return response && response.data;
    })
    .then((json) => {
      resolve(json);
    })
    .catch((ex) => {
      console.error('parsing failed', ex);
      reject(ex);
    });
});


export const GET = ({ url, params }) => sendRequest({
  serviceFunc: axios.get,
  url,
  payload: {
    params: params || {},
  },
});

export const POST = ({ url, data }) => sendRequest({
  serviceFunc: axios.post,
  url,
  payload: data,
});

export const PATCH = ({ url, data }) => sendRequest({
  serviceFunc: axios.patch,
  url,
  payload: data,
});
