import moment from 'moment/moment';

export function convertToTransactionDate(dateObj) {
  return moment(dateObj).format('YYYY-MM-DD');
}

export function getTransactionDateStartOfMonth() {
  return convertToTransactionDate(moment().startOf('month'));
}

export function getTransactionDateEndOfMonth() {
  return convertToTransactionDate(moment().endOf('month'));
}
