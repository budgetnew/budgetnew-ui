import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import DepositList from '../../components/DepositList/DepositList';
import {
  loadDepositListSagaAC,
} from '../../redux/deposits';
import { getDepositTypesSagaAC } from '../../redux/depositTypes';
import { getCurrenciesSagaAC } from '../../redux/currencies';
import { setAppBarTitleAC } from '../../redux/layout';


const PAGE_TITLE = 'Deposits';

class DepositListPage extends PureComponent {
  componentDidMount() {
    this.props.setAppBarTitleAC(PAGE_TITLE);
    this.props.getDepositTypesSagaAC();
    this.props.getCurrenciesSagaAC();
    this.props.loadDepositListSagaAC();
  }

  render() {
    const {
      deposits,
      depositTypes,
      currencies,
      loadDepositListSagaAC,
    } = this.props;
    return (
      <div>
        <DepositList
          deposits={deposits}
          depositTypes={depositTypes}
          currencies={currencies}
          getDeposits={loadDepositListSagaAC}
        />
      </div>
    );
  }
}

DepositListPage.propTypes = {
  deposits: PropTypes.arrayOf(PropTypes.object).isRequired,
  loadDepositListSagaAC: PropTypes.func.isRequired,
};

export default connect(
  state => ({
    deposits: state.deposits,
    depositTypes: state.depositTypes,
    currencies: state.currencies,
  }),
  {
    loadDepositListSagaAC,
    setAppBarTitleAC,
    getDepositTypesSagaAC,
    getCurrenciesSagaAC,
  },
)(DepositListPage);
