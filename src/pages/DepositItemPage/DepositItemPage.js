import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import DepositItem from '../../components/DepositItem/DepositItem';
import {
  loadDepositListSagaAC,
  loadDepositSagaAC,
  saveDepositSagaAC,
  getDepositByIdFunc,
} from '../../redux/deposits';
import { getDepositTypesSagaAC } from '../../redux/depositTypes';
import { getCurrenciesSagaAC } from '../../redux/currencies';
import { setAppBarTitleAC } from '../../redux/layout';


class DepositItemPage extends React.Component {
  componentDidMount() {
    const depositId = this.props.match.params.id;
    const pageTitle = `${depositId === 'new' ? 'New' : 'Edit'} Deposit`;
    this.props.setAppBarTitleAC(pageTitle);
    this.props.getDepositTypesSagaAC();
    this.props.getCurrenciesSagaAC();
    if (depositId !== 'new') {
      this.props.getDepositSagaAC(depositId);
    }
  }

  render() {
    const {
      getDepositByIdFunc,
      depositTypes,
      currencies,
      saveDepositSagaAC,
      match,
    } = this.props;
    const data = getDepositByIdFunc(match.params.id) || {};
    return (
      <DepositItem
        data={data}
        depositTypes={depositTypes}
        currencies={currencies}
        saveHandler={saveDepositSagaAC}
      />
    );
  }
}

DepositItemPage.propTypes = {
  getDepositByIdFunc: PropTypes.func.isRequired,
  getDepositsSagaAC: PropTypes.func.isRequired,
  getDepositSagaAC: PropTypes.func.isRequired,
};

export default connect(
  state => ({
    getDepositByIdFunc: getDepositByIdFunc(state),
    depositTypes: state.depositTypes,
    currencies: state.currencies,
  }),
  {
    getDepositsSagaAC: loadDepositListSagaAC,
    getDepositSagaAC: loadDepositSagaAC,
    saveDepositSagaAC,
    setAppBarTitleAC,
    getDepositTypesSagaAC,
    getCurrenciesSagaAC,
  },
)(withRouter(DepositItemPage));
