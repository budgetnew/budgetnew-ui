import React, { PureComponent, lazy, Suspense } from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { isEqual } from 'lodash';
import { compose } from 'recompose';
import {
  getAuthUserSagaAC,
  logoutSagaAC,
} from '../../redux/auth';
import {
  toggleTransactionFiltersAC,
} from '../../redux/appBar';
import DashboardPage from '../DashboardPage/DashboardPage';
import DepositListPage from '../DepositListPage/DepositListPage';
import DepositItemPage from '../DepositItemPage/DepositItemPage';
import TransactionListPage from '../TransactionListPage/TransactionListPage';
import TransactionItemPage from '../TransactionItemPage/TransactionItemPage';
import ReportsPage from '../ReportsPage/ReportsPage';
import AppBar from '../../components/AppBar/AppBar';
import AppMenu from '../../components/AppMenu/AppMenu';
import withCssPrefix from '../../hocs/withCssPrefix';
import './styles.sass';


class AppBasePage extends PureComponent {
  state = {
    appMenuOpened: false,
    transactionFiltersOpened: false,
  };

  componentDidMount() {
    this.props.getAuthUserSagaAC();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (!isEqual(prevProps.location, this.props.location)) {
      this.closeAppMenu();
    }
  }

  openAppMenu = () => {
    this.setState({
      appMenuOpened: true,
    })
  };

  closeAppMenu = () => {
    this.setState({
      appMenuOpened: false,
    })
  };

  render() {
    const {
      logoutSagaAC,
      appBarTitle,
      appBarState,
      toggleTransactionFiltersAC,
      cssPrefix,
    } = this.props;
    const { appMenuOpened } = this.state;
    return (
      <div className={cssPrefix}>
        <AppBar
          toggleAppMenu={this.openAppMenu}
          title={appBarTitle}
          toggleTransactionFilters={toggleTransactionFiltersAC}
          transactionFiltersOpened={appBarState.transactionFiltersOpened}
        />
        {
          appMenuOpened && (
            <AppMenu
              closeHandler={this.closeAppMenu}
              logoutHandler={logoutSagaAC}
            />
          )
        }
        <div className={`${cssPrefix}-body`}>
          <Suspense fallback={<div>Loading...</div>}>
            <Switch>
              <Route path="/dashboard" component={DashboardPage} />
              <Route path="/deposits/:id" component={DepositItemPage} />
              <Route path="/deposits" component={DepositListPage} />
              <Route path="/transactions/:id" component={TransactionItemPage} />
              <Route path="/transactions" component={TransactionListPage} />
              <Route path="/reports" component={ReportsPage} />
              <Redirect from="/" to="/dashboard" />
            </Switch>
          </Suspense>
        </div>
      </div>
    );
  }
}

export default compose(
  withRouter,
  withCssPrefix,
  connect(
    state => ({
      auth: state.auth,
      appBarTitle: state.layout.appBarTitle,
      appBarState: state.appBar,
      state,
    }),
    {
      getAuthUserSagaAC,
      logoutSagaAC,
      toggleTransactionFiltersAC,
    },
  )
)(AppBasePage);
