import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setAppBarTitleAC } from '../../redux/layout';
import TransactionReports from '../../components/TransactionReports/TransactionReports';
import {
  getReportDataSagaAC,
  clearReportDataAC,
  getReportTransactionsForCategorySagaAC,
} from '../../redux/reports';


const PAGE_TITLE = 'Reports';

class ReportsPage extends PureComponent {
  componentDidMount() {
    this.props.setAppBarTitleAC(PAGE_TITLE);
  }

  render() {
    const {
      reportData,
      categoryTransactions,
      getReportDataSagaAC,
      getReportTransactionsForCategorySagaAC,
      clearReportDataAC,
    } = this.props;

    return (
      <div>
        <TransactionReports
          reportData={reportData}
          categoryTransactions={categoryTransactions}
          getReportData={getReportDataSagaAC}
          clearReportData={clearReportDataAC}
          getReportTransactionsForCategory={getReportTransactionsForCategorySagaAC}
        />
      </div>
    );
  }
}

ReportsPage.propTypes = {
  getReportDataSagaAC: PropTypes.func.isRequired,
  clearReportDataAC: PropTypes.func.isRequired,
  setAppBarTitleAC: PropTypes.func.isRequired,
  reportData: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  categoryTransactions: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  isMobile: PropTypes.bool.isRequired,
};

export default connect(
  (state) => ({
    reportData: state.reports.reportData,
    categoryTransactions: state.reports.categoryTransactions,
  }),
  {
    getReportDataSagaAC,
    clearReportDataAC,
    getReportTransactionsForCategorySagaAC,
    setAppBarTitleAC,
  },
)(ReportsPage);
