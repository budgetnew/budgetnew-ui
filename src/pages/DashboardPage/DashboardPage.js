import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loadDashboardExpensesSagaAC } from '../../redux/dashboard';
import { setAppBarTitleAC } from '../../redux/layout';
import DashboardExpenses from '../../components/DashboardExpenses/DashboardExpenses';
import { TRANSACTION_TYPES } from '../../dictionaries/transactionTypes';
import { getReportTransactionsForCategorySagaAC } from '../../redux/reports';
import {
  getTransactionDateEndOfMonth,
  getTransactionDateStartOfMonth
} from '../../utils/date.utils';


const PAGE_TITLE = 'Dashboard';

class DashboardPage extends PureComponent {
  componentDidMount() {
    const {
      setAppBarTitle,
      loadDashboardExpenses,
    } = this.props;
    setAppBarTitle(PAGE_TITLE);
    const fromDateMoment = getTransactionDateStartOfMonth();
    const toDateMoment = getTransactionDateEndOfMonth();
    loadDashboardExpenses({
      transactionTypeId: TRANSACTION_TYPES.EXPENSE,
      fromDate: fromDateMoment,
      toDate: toDateMoment,
      groupBy: 'category',
    });
  }

  loadCategoryExpensesIfAbsent = (categoryId) => {
    const {
      categoryTransactions,
      getReportTransactionsForCategory,
    } = this.props;
    if (!categoryTransactions[categoryId]) {
      getReportTransactionsForCategory({
        fromDate: getTransactionDateStartOfMonth(),
        toDate: getTransactionDateEndOfMonth(),
        categoryId,
        transactionTypeId: TRANSACTION_TYPES.EXPENSE,
      });
    }
  };

  render() {
    const {
      expenses,
      categoryTransactions,
    } = this.props;

    return (
      <div>
        <DashboardExpenses
          expenses={expenses}
          loadCategoryExpensesIfAbsent={this.loadCategoryExpensesIfAbsent}
          categoryTransactions={categoryTransactions}
        />
      </div>
    );
  }
}

DashboardPage.propTypes = {
  setAppBarTitle: PropTypes.func.isRequired,
  loadDashboardExpenses: PropTypes.func.isRequired,
  getReportTransactionsForCategory: PropTypes.func.isRequired,
  expenses: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  categoryTransactions: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};

export default connect(
  state => ({
    expenses: state.dashboard.expenses,
    categoryTransactions: state.reports.categoryTransactions,
  }),
  {
    setAppBarTitle: setAppBarTitleAC,
    loadDashboardExpenses: loadDashboardExpensesSagaAC,
    getReportTransactionsForCategory: getReportTransactionsForCategorySagaAC,
  },
)(DashboardPage);
