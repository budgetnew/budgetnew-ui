import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import queryString from 'query-string/index';
import TransactionItem from '../../components/TransactionItem/TransactionItem';
import {
  loadDepositListSagaAC,
  saveDepositSagaAC,
} from '../../redux/deposits';
import {
  getTransactionByIdFunc,
  saveTransactionSagaAC,
  loadTransactionSagaAC
} from '../../redux/transactions';
import { loadCategoriesSagaAC } from '../../redux/categories';
import { getCurrenciesSagaAC } from '../../redux/currencies';
import { setAppBarTitleAC } from '../../redux/layout';
import TransactionTypeTabs from "../../components/TransactionTypeTabs/TransactionTypeTabs";
import { loadTransactionTypesSagaAC } from '../../redux/transactionTypes';


class TransactionItemPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      transactionTypeId: null,
    };
    this.transactionId = this.props.match.params.id;
    const parsedQueryParams = queryString.parse(this.props.location.search);
    this.useQrScanner = parsedQueryParams.scan;
  }


  componentDidMount() {
    const pageTitle = `${this.transactionId === 'new' ? 'New' : 'Edit'} Transaction`;
    this.props.setAppBarTitleAC(pageTitle);
    this.props.loadDepositListSagaAC();
    this.props.loadTransactionTypesSagaAC();
    if (this.transactionId !== 'new') {
      this.props.loadTransactionSagaAC(this.transactionId);
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (!this.state.transactionTypeId) {
      if (this.transactionId === 'new') {
        if (!this.state.transactionTypeId && this.props.transactionTypeList.length > 0) {
          this.selectTransactionTypeHandler(this.props.transactionTypeList[0].id);
        }
      } else {
        const data = this.props.getTransactionById(this.transactionId);

        if (this.props.transactionTypeList.length > 0 && data?.transactionTypeId) {
          this.selectTransactionTypeHandler(data.transactionTypeId);
        }
      }
    }
  }

  selectTransactionTypeHandler = (transactionTypeId) => {
    this.setState({
      transactionTypeId,
    });
    this.props.loadCategoriesSagaAC(transactionTypeId);
  };

  render() {
    const {
      getTransactionById,
      depositList,
      categoryList,
      loadCategoriesSagaAC,
      transactionTypeList,
      transactionItem,
      saveTransactionSagaAC,
    } = this.props;
    const { transactionTypeId } = this.state;
    const data = getTransactionById(this.transactionId);
    return (
      <div>
        <TransactionTypeTabs
          transactionTypeList={transactionTypeList}
          selectedId={transactionTypeId}
          onSelect={this.selectTransactionTypeHandler}
          disabled={this.transactionId !== 'new'}
        />
        <TransactionItem
          data={data}
          depositList={depositList}
          categoryList={categoryList}
          loadCategories={loadCategoriesSagaAC}
          saveHandler={saveTransactionSagaAC}
          transactionTypeId={transactionTypeId}
          readonly={this.transactionId !== 'new'}
          useQrScanner={this.useQrScanner}
          formShouldReset={transactionItem.formShouldReset}
          status={transactionItem.status}
        />
      </div>

    );
  }
}

TransactionItemPage.propTypes = {
  getDepositByIdFunc: PropTypes.func.isRequired,
  loadDepositListSagaAC: PropTypes.func.isRequired,
  loadCategoriesSagaAC: PropTypes.func.isRequired,
  saveTransactionSagaAC: PropTypes.func.isRequired,
};

export default connect(
  state => ({
    getTransactionById: getTransactionByIdFunc(state),
    transactionTypeList: state.transactionTypes,
    transactionItem: state.transactions.item,
    depositList: state.deposits,
    categoryList: state.categories,
  }),
  {
    loadDepositListSagaAC,
    loadTransactionSagaAC,
    loadTransactionTypesSagaAC,
    saveTransactionSagaAC,
    saveDepositSagaAC,
    setAppBarTitleAC,
    loadCategoriesSagaAC,
    getCurrenciesSagaAC,
  },
)(withRouter(TransactionItemPage));
