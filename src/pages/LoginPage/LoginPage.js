import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import LoginForm from '../../components/LoginForm/LoginForm';
import { loginSagaAC, getAuthUserSagaAC } from '../../redux/auth';
import './LoginPage.sass';
import withCssPrefix from '../../hocs/withCssPrefix';


class LoginPage extends PureComponent {
  componentDidMount() {
    this.props.getAuthUserSagaAC();
  }

  render() {
    const {
      auth,
      loginSagaAC,
      cssPrefix,
    } = this.props;
    return (
      <div className={cssPrefix}>
        <div className={`${cssPrefix}-appNameWrapper`}>
          <div className={`${cssPrefix}-appName`}>Budgee</div>
          <div className={`${cssPrefix}-appSubtitle`}>Keep your expenses under control</div>
        </div>
        <LoginForm
          auth={auth}
          loginHandler={loginSagaAC}
        />
      </div>
    );
  }
}


export default compose(
  withCssPrefix,
  connect(
    (state) => ({
      auth: state.auth,
    }),
    {
      loginSagaAC,
      getAuthUserSagaAC,
    },
  ),
)(LoginPage);
