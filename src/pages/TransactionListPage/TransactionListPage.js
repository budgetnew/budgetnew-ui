import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import {
  loadTransactionListSagaAC,
} from '../../redux/transactions';

import { loadCategoriesSagaAC } from '../../redux/categories';
import { setAppBarTitleAC } from '../../redux/layout';
import TransactionList from '../../components/TransactionList/TransactionList';
import { loadTransactionTypesSagaAC } from '../../redux/transactionTypes';
import TransactionListFilters from '../../components/TransactionListFilters/TransactionListFilters';
import { closeTransactionFiltersAC } from '../../redux/appBar';


const PAGE_TITLE = 'Transactions';

class TransactionListPage extends PureComponent {
  state = {
    filters: {},
  };

  componentDidMount() {
    this.props.setAppBarTitleAC(PAGE_TITLE);
    this.props.loadTransactionListSagaAC();
    this.props.loadCategoriesSagaAC();
  }

  componentWillUnmount() {
    this.props.closeTransactionFiltersAC();
  }

  loadTransactions = (filters) => {
    this.props.loadTransactionListSagaAC(filters);
    this.setState({
      filters
    });
  };

  render() {
    const {
      transactions,
      deposits,
      allCategories,
      appBarState,
      closeTransactionFiltersAC,
    } = this.props;
    const { filters } = this.state;

    return (
      <div>
        {
          appBarState.transactionFiltersOpened && (
            <TransactionListFilters
              allCategoryList={allCategories}
              filters={filters}
              onClose={this.loadTransactions}
              closeHandler={closeTransactionFiltersAC}
            />
          )
        }
        <TransactionList
          transactions={transactions}
          deposits={deposits}
        />
      </div>
    );
  }
}

export default connect(
  state => ({
    transactions: state.transactions.list,
    transactionTypeList: state.transactionTypes,
    deposits: state.deposits,
    allCategories: state.categories,
    appBarState: state.appBar,
  }),
  {
    loadTransactionListSagaAC,
    loadTransactionTypesSagaAC,
    setAppBarTitleAC,
    loadCategoriesSagaAC,
    closeTransactionFiltersAC,
  },
)(TransactionListPage);
