import { takeEvery, put } from 'redux-saga/effects';
import { GET, POST } from '../../utils/serviceHelper';
import { LOG_IN, LOG_OUT, GET_AUTH_USER } from '../../redux/auth';
import { SUCCESS } from '../../dictionaries/actionPostfix';
import { getServerDataDeprecated, getServerData } from '../utils';
import history from '../../routes/history';


function* authenticateSaga(action) {
  yield getServerData({
    serviceFunc: POST,
    serviceArgs: { url: '/login', data: action.data },
    action: LOG_IN,
    errorMessage: 'Authentication error',
    successClb: () => { history.push('/'); },
  });
}

function* getAuthUserSaga() {
  yield getServerDataDeprecated(
    GET,
    { url: '/getLoggedIn' },
    GET_AUTH_USER,
  );
}

function* logoutSaga() {
  yield getServerDataDeprecated(
    POST,
    { url: '/logout' },
    LOG_OUT,
  );
  yield put({ type: LOG_OUT + SUCCESS });
}

export function* loginSagaWatcher() {
  yield takeEvery(LOG_IN, authenticateSaga);
}

export function* getAuthUserSagaWatcher() {
  yield takeEvery(GET_AUTH_USER, getAuthUserSaga);
}

export function* logoutSagaWatcher() {
  yield takeEvery(LOG_OUT, logoutSaga);
}
