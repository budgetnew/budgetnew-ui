import { all } from 'redux-saga/effects';
import { loginSagaWatcher, logoutSagaWatcher, getAuthUserSagaWatcher } from './auth/authSaga';
import {
  getDepositsSagaWatcher,
  getDepositSagaWatcher,
  saveDepositSagaWatcher,
} from './deposits/depositsSaga';
import {
  transactionsSagaWatcher,
  getTransactionSagaWatcher,
  saveTransactionSagaWatcher,
} from './transactions/transactionsSaga';
import transactionTypesSagaWatcher from './transactionTypes/transactionTypesSaga';
import categoriesSagaWatcher from './categories/categoriesSaga';
import dashboardSagaWatcher from './dashboard/dashboard';
import depositTypesWatcher from './depositTypes/depositTypesSaga';
import currenciesWatcher from './currencies/currenciesSaga';
import getReportData from './reports/reports';
import getReportTransactionsForCategoryWatcher from './reports/getReportTransactionsForCategorySaga';
import { getTagsSagaWatcher } from './tags/tagsSaga';


export default function* () {
  yield all([
    getDepositsSagaWatcher(),
    getDepositSagaWatcher(),
    saveDepositSagaWatcher(),
    loginSagaWatcher(),
    logoutSagaWatcher(),
    getAuthUserSagaWatcher(),
    transactionsSagaWatcher(),
    getTransactionSagaWatcher(),
    saveTransactionSagaWatcher(),
    transactionTypesSagaWatcher(),
    categoriesSagaWatcher(),
    dashboardSagaWatcher(),
    depositTypesWatcher(),
    currenciesWatcher(),
    getReportData(),
    getReportTransactionsForCategoryWatcher(),
    getTagsSagaWatcher(),
  ]);
}
