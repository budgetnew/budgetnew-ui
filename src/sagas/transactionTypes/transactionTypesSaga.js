import { takeEvery } from 'redux-saga/effects';
import { GET } from '../../utils/serviceHelper';
import { LOAD_TRANSACTION_TYPE_LIST } from '../../redux/transactionTypes';
import { getServerData } from '../utils';

function* loadTrasactionTypeListSaga() {
  yield getServerData({
    serviceFunc: GET,
    serviceArgs: {
      url: '/transactionTypes',
    },
    action: LOAD_TRANSACTION_TYPE_LIST,
  });
}

export default function* () {
  yield takeEvery(LOAD_TRANSACTION_TYPE_LIST, loadTrasactionTypeListSaga);
}
