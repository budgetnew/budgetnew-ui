import { call, put } from 'redux-saga/effects';
import { START, SUCCESS } from '../dictionaries/actionPostfix';
import { showSnackMessageSagaAC } from '../redux/snackMessage';
import history from '../routes/history';


export function* getServerDataDeprecated(serviceFunc, serviceArgs, action, successMessage, errorMessage) {
  yield getServerData({
    serviceFunc,
    serviceArgs,
    action,
    successMessage,
    errorMessage
  });
}

export function* getServerData({ serviceFunc, serviceArgs, action, successMessage, errorMessage, successClb }) {
  try {
    yield put({ type: action + START, data, serviceArgs });
    const data = yield call(serviceFunc, serviceArgs);
    yield put({ type: action + SUCCESS, data, serviceArgs });
    if (successMessage) {
      yield put(showSnackMessageSagaAC(successMessage, 'success'));
    }
    if (successClb) {
      successClb(data);
    }
  } catch (e) {
    console.error(e);
    if (e.response?.status === 401 && history.location?.pathname.indexOf('/login') !== 0) {
      return history.push('/login');
    }
    if (errorMessage) {
      yield put(showSnackMessageSagaAC(errorMessage, 'error'));
    }
  }
}
