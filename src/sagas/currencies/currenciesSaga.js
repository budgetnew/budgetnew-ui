import { takeEvery } from 'redux-saga/effects';
import { GET } from '../../utils/serviceHelper';
import { GET_CURRENCIES } from '../../redux/currencies';
import { getServerDataDeprecated } from '../utils';

function* currenciesSaga() {
  yield getServerDataDeprecated(GET, {
    url: '/currencies',
  }, GET_CURRENCIES);
}

export default function* () {
  yield takeEvery(GET_CURRENCIES, currenciesSaga);
}
