import { takeEvery } from 'redux-saga/effects';
import history from '../../routes/history';
import { GET, POST, PATCH } from '../../utils/serviceHelper';
import {
  GET_DEPOSITS,
  GET_DEPOSIT,
  SAVE_DEPOSIT,
} from '../../redux/deposits';
import { getServerDataDeprecated, getServerData } from '../utils';

function* getDepositsSaga() {
  yield getServerDataDeprecated(GET, {
    url: '/deposits',
  }, GET_DEPOSITS);
}

function* getDepositSaga({ payload: { id } }) {
  yield getServerDataDeprecated(GET, {
    url: `/deposits/${id}`,
  }, GET_DEPOSIT);
}

function* saveDepositSaga({ data }) {
  const {
    id,
    ...restData
  } = data;
  const serviceFunc = id ? PATCH : POST;
  const url = id ? `/deposits/${id}` : '/deposits';
  yield getServerData({
    serviceFunc,
    serviceArgs: {
      url,
      data: restData,
    },
    action: SAVE_DEPOSIT,
    successMessage: 'Deposit successfully saved',
    errorMessage: 'Error while saving a deposit',
    successClb: () => {
      history.push('/deposits');
    },
  });
}

export function* getDepositsSagaWatcher() {
  yield takeEvery(GET_DEPOSITS, getDepositsSaga);
}

export function* getDepositSagaWatcher() {
  yield takeEvery(GET_DEPOSIT, getDepositSaga);
}

export function* saveDepositSagaWatcher() {
  yield takeEvery(SAVE_DEPOSIT, saveDepositSaga);
}
