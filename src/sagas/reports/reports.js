import { takeEvery } from 'redux-saga/effects';
import { GET } from '../../utils/serviceHelper';
import { GET_REPORT_DATA } from '../../redux/reports';
import { getServerDataDeprecated } from '../utils';

function* getReportDataSaga({ data }) {
  yield getServerDataDeprecated(GET, {
    url: '/transactions',
    params: data,
  }, GET_REPORT_DATA);
}

export default function* () {
  yield takeEvery(GET_REPORT_DATA, getReportDataSaga);
}
