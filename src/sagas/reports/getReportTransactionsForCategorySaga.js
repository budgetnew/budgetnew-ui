import { takeEvery } from 'redux-saga/effects';
import { GET } from '../../utils/serviceHelper';
import { GET_REPORT_TRANSACTIONS_FOR_CATEGORY } from '../../redux/reports';
import { getServerDataDeprecated } from '../utils';


function* getReportTransactionsForCategorySaga({ data }) {
  yield getServerDataDeprecated(GET, {
    url: '/transactions',
    params: data,
  }, GET_REPORT_TRANSACTIONS_FOR_CATEGORY);
}

export default function* () {
  yield takeEvery(GET_REPORT_TRANSACTIONS_FOR_CATEGORY, getReportTransactionsForCategorySaga);
}
