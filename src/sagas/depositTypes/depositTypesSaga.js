import { takeEvery } from 'redux-saga/effects';
import { GET } from '../../utils/serviceHelper';
import { GET_DEPOSIT_TYPES } from '../../redux/depositTypes';
import { getServerDataDeprecated } from '../utils';

function* depositTypesSaga() {
  yield getServerDataDeprecated(GET, {
    url: '/depositTypes',
  }, GET_DEPOSIT_TYPES);
}

export default function* () {
  yield takeEvery(GET_DEPOSIT_TYPES, depositTypesSaga);
}
