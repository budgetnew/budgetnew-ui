import { takeEvery } from 'redux-saga/effects';
import { GET } from '../../utils/serviceHelper';
import { LOAD_CATEGORY_LIST } from '../../redux/categories';
import { getServerDataDeprecated } from '../utils';

function* categoriesSaga({ payload: { transactionTypeId } }) {
  yield getServerDataDeprecated(GET, {
    url: '/categories',
    params: { transactionTypeId },
  }, LOAD_CATEGORY_LIST);
}

export default function* () {
  yield takeEvery(LOAD_CATEGORY_LIST, categoriesSaga);
}
