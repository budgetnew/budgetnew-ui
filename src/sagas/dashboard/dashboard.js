import { takeEvery } from 'redux-saga/effects';
import { GET } from '../../utils/serviceHelper';
import { LOAD_DASHBOARD_EXPENSES } from '../../redux/dashboard';
import { getServerDataDeprecated } from '../utils';

function* dashboardExpensesSaga({ data }) {
  yield getServerDataDeprecated(GET, {
    url: '/transactions',
    params: data,
  }, LOAD_DASHBOARD_EXPENSES);
}

export default function* () {
  yield takeEvery(LOAD_DASHBOARD_EXPENSES, dashboardExpensesSaga);
}
