import { takeEvery, call } from 'redux-saga/effects';
import { GET, POST } from '../../utils/serviceHelper';
import { GET_TRANSACTIONS, LOAD_TRANSACTION, SAVE_TRANSACTION } from '../../redux/transactions';
import { getServerDataDeprecated, getServerData } from '../utils';
import history from '../../routes/history';


function* loadTransactionListSaga({ data }) {
  yield getServerDataDeprecated(GET, {
    url: '/transactions',
    params: data,
  }, GET_TRANSACTIONS);
}

function* getTransactionSaga({ payload: { id } }) {
  yield call(getServerData, {
    serviceFunc: GET,
    serviceArgs: {
      url: `/transactions/${id}`,
    },
    action: LOAD_TRANSACTION,
  });
}

function* saveTransactionSaga({ data }) {
  yield getServerData({
    serviceFunc: POST,
    serviceArgs: {
      url: '/transactions',
      data,
    },
    action: SAVE_TRANSACTION,
    successMessage: 'Transaction successfully created',
    errorMessage: 'Error while creating transaction',
    // successClb: () => {
    //   history.push('/transactions');
    // },
  });
}

export function* transactionsSagaWatcher() {
  yield takeEvery(GET_TRANSACTIONS, loadTransactionListSaga);
}

export function* getTransactionSagaWatcher() {
  yield takeEvery(LOAD_TRANSACTION, getTransactionSaga);
}

export function* saveTransactionSagaWatcher() {
  yield takeEvery(SAVE_TRANSACTION, saveTransactionSaga);
}
