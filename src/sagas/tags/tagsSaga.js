import { takeEvery } from 'redux-saga/effects';
import { GET, POST } from '../../utils/serviceHelper';
import {
  GET_TAGS,
} from '../../redux/tags';
import { getServerDataDeprecated } from '../utils';

function* getTagsSaga() {
  yield getServerDataDeprecated(GET, {
    url: '/tags',
  }, GET_TAGS);
}

// function* saveDepositSaga({ data }) {
//   yield getServerDataDeprecated(POST, {
//     url: '/deposits',
//     data,
//   }, SAVE_DEPOSIT, 'Deposit successfully saved', 'Error while saving a deposit');
//   yield getDepositsSaga();
// }

export function* getTagsSagaWatcher() {
  yield takeEvery(GET_TAGS, getTagsSaga);
}

// export function* saveDepositSagaWatcher() {
//   yield takeEvery(SAVE_DEPOSIT, saveDepositSaga);
// }
