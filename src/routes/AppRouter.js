import React from 'react';
import {
  Router,
  Switch,
  Route,
} from 'react-router-dom';
import { toast } from 'react-toastify';
import history from './history';
import Script from 'react-load-script';

import AppBasePage from '../pages/AppBasePage/AppBasePage';
import LoginPage from '../pages/LoginPage/LoginPage';
import config from '../../config';


toast.configure({
  position: 'bottom-left',
  autoClose: 5000,
  hideProgressBar: false,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  closeButton: <i className="material-icons Toastify__closeButton">close</i>,
});


class AppRouter extends React.Component {
  handleScriptLoad = () => {
    gapi.load('auth2');
  };

  render() {
    return (
      <React.Fragment>
        <Script
          url={`https://apis.google.com/js/platform.js?key=${config.googleApiClientId}`}
          // onCreate={this.handleScriptCreate.bind(this)}
          // onError={this.handleScriptError.bind(this)}
          onLoad={this.handleScriptLoad}
        />
        <Router history={history}>
          <main>
            <Switch>
              <Route path="/login" component={LoginPage} />
              <Route path="/" component={AppBasePage} />
            </Switch>
          </main>
        </Router>
      </React.Fragment>

    );
  }
}

export default AppRouter;
