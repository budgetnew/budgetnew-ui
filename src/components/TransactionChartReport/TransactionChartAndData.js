import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import HighchartsReact from 'highcharts-react-official';
import memoizeOne from 'memoize-one';
import Highcharts from '../../basicComponents/Highcharts/Highcharts';
import withCssPrefix from '../../hocs/withCssPrefix';
import './TransactionChartAndData.sass';


class TransactionChartAndData extends PureComponent {
  getChartOptions = memoizeOne((data, categoryClickHandler) => {
    const seriesData = data
      .map(entry => ({
        name: entry.category.name,
        y: Math.round(entry.sumFrom),
        categoryId: entry.categoryId,
      }));

    const categories = seriesData.map(entry => entry.name);

    return {
      chart: {
        type: 'column',
      },
      title: {
        text: 'Expenses Report',
      },
      tooltip: {
        pointFormat: '<b>{point.y} руб.</b>',
      },
      xAxis: {
        categories,
        labels: {
          x: -10
        }
      },
      yAxis: {
        allowDecimals: false,
        title: {
          text: 'Amount'
        }
      },
      series: [{
        data: seriesData,
      }],
      legend: {
        enabled: false,
      },
      plotOptions: {
        column: {
          allowPointSelect: true,
          cursor: 'pointer',
          events: {
            click: (node) => { categoryClickHandler(node.point.categoryId); },
          },
        },
      },
    };

  });

  renderCategoryTransaction = (categoryId) => {
    const {
      categoryTransactions,
      selectedCategoryId,
      cssPrefix,
    } = this.props;
    if (!selectedCategoryId || selectedCategoryId !== categoryId) {
      return null;
    }
    const selectedCategoryTransactions = categoryTransactions[categoryId];

    return !!selectedCategoryTransactions && selectedCategoryTransactions.length > 0 && (
      <div className={`${cssPrefix}-categoryTransactions`}>
        {selectedCategoryTransactions
          .sort((a, b) => b.valueFrom - a.valueFrom)
          .map(row => (
            <div
              key={row.id}
              className={`${cssPrefix}-categoryTransactionsItem`}
            >
              <span>{row.comment}</span><span>{row.valueFrom}</span>
            </div >
          ))
        }
      </div>
    );
  };

  getTotalAmountSpent() {
    const {
      data,
    } = this.props;
    return data.reduce((accumulator, v) => accumulator + parseFloat(v.sumFrom), 0);
  }

  render() {
    const {
      data,
      categoryClickHandler,
      cssPrefix,
    } = this.props;
    const chartOptions = this.getChartOptions(data, categoryClickHandler);
    const totalAmountSpent = this.getTotalAmountSpent();

    return (
      <div className={`${cssPrefix}-chartWrapper`}>
        <div>
          <HighchartsReact
            highcharts={Highcharts}
            options={chartOptions}
          />
        </div>
        {
          totalAmountSpent > 0 && (
            <div className={`${cssPrefix}-totalCount`}>
              <span>Total spent</span><span>{this.getTotalAmountSpent()}</span>
            </div>
          )
        }
        <div className={`${cssPrefix}-categoryList`}>
          {data
            // .sort((a, b) => b.sumFrom - a.sumFrom)
            .map(row => (
              <div
                key={row.category.name}
                className={`${cssPrefix}-categoryItem`}
              >
                <div className={`${cssPrefix}-categoryTotal`}>
                  <span>{row.category.name}</span><span>{row.sumFrom}</span>
                </div>
                {
                  this.renderCategoryTransaction(row.category.id)
                }
              </div>
            ))
          }
        </div>
      </div>
    )
  }
}

TransactionChartAndData.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    sumFrom: PropTypes.string,
    category: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    }),
  })).isRequired,
  categoryTransactions: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    comment: PropTypes.string,
    valueFrom: PropTypes.string,
  })),
  categoryClickHandler: PropTypes.func,
  selectedCategoryId: PropTypes.number,
};

TransactionChartAndData.defaultProps = {
  categoryTransactions: [],
  categoryClickHandler: () => null,
  selectedCategoryId: null,
};


export default withCssPrefix(TransactionChartAndData);
