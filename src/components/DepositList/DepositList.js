import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import FloatingButton from '../FloatingButton/FloatingButton';
import withCssPrefix from '../../hocs/withCssPrefix';
import './styles.sass';


class DepositList extends PureComponent {
  constructor(props) {
    super(props);

    window.qwe = React.createRef();

  }

  navigateToDepositPageFunc = (depositId) => () => {
    this.props.history.push(`/deposits/${depositId}`)
  };

  renderDepositIcon = (deposit) => {
    const typeName = deposit?.depositType?.name;
    let materialIconName;
    switch (typeName) {
      case 'credit card':
        materialIconName = 'credit_card';
        break;
      case 'cash':
        materialIconName = 'account_balance_wallet';
        break;
      case 'e-money':
        materialIconName = 'monetization_on';
        break;
      default:
        materialIconName = '';
    }
    return (
      <i className="material-icons">{materialIconName}</i>
    );
  };

  render() {
    const {
      deposits,
      cssPrefix,
    } = this.props;
    return (
      <div className={cssPrefix}>
        {
          deposits.length > 0 && deposits.map(deposit => (
            <div
              key={deposit.id}
              className={`${cssPrefix}-item`}
              onClick={this.navigateToDepositPageFunc(deposit.id)}
            >
              <div className={`${cssPrefix}-itemIconWrapper`}>
                { this.renderDepositIcon(deposit) }
              </div>
              <div className={`${cssPrefix}-itemInfo`}>
                <div className={`${cssPrefix}-itemName`}>
                  {deposit.name}
                </div>
                <div className={`${cssPrefix}-itemBalance`}>
                  {
                    deposit?.currency?.symbol && (
                      <>
                        <span dangerouslySetInnerHTML={{ __html: deposit.currency.symbol}} />
                        &nbsp;
                      </>
                    )
                  }
                  {deposit.balance}</div>
              </div>
              <i ref={r => window.qwe = r} className={`material-icons ${cssPrefix}-visibility`}>visibility</i>
            </div>
          ))
        }
        <FloatingButton
          onClick={this.navigateToDepositPageFunc('new')}
          icon="add"
        />
      </div>
    );
  }
}

DepositList.propTypes = {
  deposits: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    balance: PropTypes.string.isRequired,
    depositType: PropTypes.shape({}),
    currency: PropTypes.shape({}),
  })).isRequired,
};

export default compose(
  withRouter,
  withCssPrefix,
)(DepositList);
