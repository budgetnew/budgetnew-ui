import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import { TRANSACTION_TYPES } from '../../dictionaries/transactionTypes';
import SelectField from '../../basicComponents/SelectField/SelectField';
import InputField from '../../basicComponents/InputField/InputField';
import withCssPrefix from '../../hocs/withCssPrefix';
import './styles.sass'
import classnames from 'classnames';
import { convertToTransactionDate } from '../../utils/date.utils';


const transactionTypeOptionList = Object.keys(TRANSACTION_TYPES).map(type => ({
  value: TRANSACTION_TYPES[type],
  label: type.toLowerCase(),
}));

const defaultState = {
  transactionTypeId: null,
  categoryId: null,
  fromDate: '',
  toDate: '',
};

class TransactionListFilters extends PureComponent {
  state = defaultState;

  componentDidMount() {
    if (!isEmpty(this.props.filters)) {
      const {
        transactionTypeId,
        categoryId,
        fromDate,
        toDate,
      } = this.props.filters;

      this.setState({
        transactionTypeId,
        categoryId,
        fromDate: fromDate ? convertToTransactionDate(fromDate) : null,
        toDate: toDate ? convertToTransactionDate(toDate) : null,
      });
    }
  }

  componentWillUnmount() {
    const {
      transactionTypeId,
      categoryId,
      fromDate,
      toDate,
    } = this.state;
    this.props.onClose({
      transactionTypeId,
      categoryId,
      fromDate: fromDate && convertToTransactionDate(fromDate),
      toDate: toDate && convertToTransactionDate(toDate),
    });
  }

  setDefaultState = () => {
    this.setState(defaultState);
  };

  setParam = ({ target: { name, value } }) => {
    this.setState({
      [name]: value,
    })
  };

  setSelectParam = (fieldName) => (option) => {
    const newState = {
      [fieldName]: option.value,
    };
    if (fieldName === 'transactionTypeId') {
      newState.categoryId = null;
    }
    this.setState(newState);
  };

  render() {
    const {
      transactionTypeId,
      categoryId,
      fromDate,
      toDate,
    } = this.state;

    const {
      allCategoryList,
      cssPrefix,
      closeHandler,
    } = this.props;

    return (
      <div className={cssPrefix}>
        <div className={`${cssPrefix}-inner`}>
          <SelectField
            value={transactionTypeId}
            className={`${cssPrefix}-field`}
            options={transactionTypeOptionList}
            label="Source Deposit"
            onChange={this.setSelectParam('transactionTypeId')}
          />
          <SelectField
            value={categoryId}
            className={`${cssPrefix}-field`}
            options={allCategoryList
              .filter(category => category.transactionTypeId === transactionTypeId)
              .map(category => ({
                value: category.id,
                label: category.name,
              }))
            }
            label="Category"
            onChange={this.setSelectParam('categoryId')}
          />
          <InputField
            name="fromDate"
            type="date"
            value={fromDate}
            onChange={this.setParam}
            label="Date from"
            className="transactionItem-field"
          />
          <InputField
            name="toDate"
            type="date"
            value={toDate}
            onChange={this.setParam}
            label="Date to"
            className="transactionItem-field"
          />
          <div className={`${cssPrefix}-buttons`}>
            <button
              className={`${cssPrefix}-clearButton`}
              onClick={this.setDefaultState}
            >
              Clear all
            </button>
          </div>
          <button
            className={classnames(
              `${cssPrefix}-applyButton`,
            )}
            onClick={closeHandler}
          >Apply</button>
        </div>
      </div>
    );
  }
}

TransactionListFilters.propTypes = {
  allCategoryList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    transactionTypeId: PropTypes.number,
  })),
  onClose: PropTypes.func.isRequired,
  closeHandler: PropTypes.func.isRequired,
};

export default withCssPrefix(TransactionListFilters);
