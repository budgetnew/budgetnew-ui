import React from 'react';
import PropTypes from 'prop-types';
import { isEqual } from 'lodash';
import querystring from 'querystring';
import classnames from 'classnames';
import InputField from "../../basicComponents/InputField/InputField";
import SelectField from "../../basicComponents/SelectField/SelectField";
import { TRANSACTION_TYPES } from '../../dictionaries/transactionTypes';
import QrScanner from '../QrScanner/QrScanner';
import withCssPrefix from '../../hocs/withCssPrefix';
import statuses from '../../dictionaries/statuses';
import './styles.sass';
import { convertToTransactionDate } from '../../utils/date.utils';


const defaultState = {
  fromDepositId: null,
  toDepositId: null,
  categoryId: null,
  comment: '',
  valueFrom: '',
  valueTo: '',
  transactionDate: convertToTransactionDate(),
  qrCodeScanned: false,
  resetFormAfterSave: true,
  validationErrors: {
    fromDepositId: '',
    toDepositId: '',
    comment: '',
    valueTo: '',
  }
};

class TransactionItem extends React.PureComponent {
  state = defaultState;

  componentDidMount() {
    const { data } = this.props;
    if (data) {
      this.setFormData(data);
    }
  }

  componentDidUpdate(prevProps) {
    const { data, transactionTypeId, formShouldReset } = this.props;

    if (formShouldReset && !prevProps.formShouldReset) {
      return this.resetFormForAnotherSave();
    }

    if (data) {
      if (!isEqual(prevProps.data, data)) {
        this.setFormData(data);
      }
    } else if (!isEqual(prevProps.transactionTypeId, transactionTypeId) && !data) {
      this.setFormData();
    }
  }

  setFormData = (data = {}) => {
    this.setState({
      ...defaultState,
      fromDepositId: data.fromDepositId || null,
      toDepositId: data.toDepositId || null,
      categoryId: data.categoryId || null,
      comment: data.comment || '',
      valueFrom: data.valueFrom ? parseFloat(data.valueFrom) : '',
      valueTo: data.valueTo ? parseFloat(data.valueTo) : '',
      transactionDate: data.transactionDate ? convertToTransactionDate(data.transactionDate) : convertToTransactionDate(),
    });
  };

  resetFormForAnotherSave = () => {
    const {
      comment,
      valueFrom,
      valueTo,
    } = defaultState;
    this.setState({
      comment,
      valueFrom,
      valueTo,
    });
  };

  setParam = ({ target: { name, value } }) => {
    const { transactionTypeId } = this.props;

    const stateUpdate = {
      [name]: value,
    };
    if (transactionTypeId === TRANSACTION_TYPES.TRANSFER && name === 'valueFrom') {
      stateUpdate.valueTo = value;
    }
    this.setState(stateUpdate);
  };

  setSelectParam = (fieldName) => (option) => {
    this.setState({
      [fieldName]: option.value,
    })
  };

  validateForm = () => {
    const {
      fromDepositId,
      toDepositId,
      categoryId,
      comment,
      valueFrom,
      valueTo,
    } = this.state;
    const { transactionTypeId } = this.props;
    const validationErrors = {};
    if ([TRANSACTION_TYPES.EXPENSE, TRANSACTION_TYPES.TRANSFER].includes(transactionTypeId)) {
      if (!fromDepositId) {
        validationErrors.fromDepositId = 'Deposit cannot be empty'
      }
      if (!valueFrom) {
        validationErrors.valueFrom = 'Value cannot be empty or zero'
      }
      if (valueFrom < 0) {
        validationErrors.valueFrom = 'Value cannot be negative'
      }
    }
    if ([TRANSACTION_TYPES.INCOME, TRANSACTION_TYPES.TRANSFER].includes(transactionTypeId)) {
      if (!toDepositId) {
        validationErrors.toDepositId = 'Deposit cannot be empty'
      }
      if (!valueTo) {
        validationErrors.valueTo = 'Value cannot be empty or zero'
      }
      if (valueTo < 0) {
        validationErrors.valueTo = 'Value cannot be negative'
      }
    }
    if ([TRANSACTION_TYPES.EXPENSE, TRANSACTION_TYPES.INCOME].includes(transactionTypeId)) {
      if (!categoryId) {
        validationErrors.categoryId = 'Category cannot be empty'
      }
    }
    // if (!comment.trim()) {
    //   validationErrors.comment = 'Comment cannot be empty'
    // }
    this.setState({
      validationErrors,
    });
    return Object.keys(validationErrors).length === 0;
  };

  qrScanSuccessHandler = (dataStr) => {
    const data = querystring.parse(dataStr);
    this.setState({
      qrCodeScanned: true,
      valueFrom: parseFloat(data.s),
      transactionDate: convertToTransactionDate(data.t),
    });
  };

  qrScanErrorHandler = (data) => {
    console.error('error', data);
  };

  save = () => {
    const {
      fromDepositId,
      toDepositId,
      categoryId,
      comment,
      valueFrom,
      valueTo,
      transactionDate,
    } = this.state;
    const {
      transactionTypeId,
      saveHandler,
      data,
    } = this.props;
    if (this.validateForm()) {
      saveHandler({
        id: data?.id,
        transactionTypeId,
        fromDepositId,
        toDepositId,
        categoryId,
        comment,
        valueFrom,
        valueTo,
        transactionDate: convertToTransactionDate(transactionDate),
      });
    }
  };

  render() {
    const {
      fromDepositId,
      toDepositId,
      categoryId,
      comment,
      valueFrom,
      valueTo,
      transactionDate,
      validationErrors,
      qrCodeScanned,
    } = this.state;
    const {
      depositList,
      categoryList,
      transactionTypeId,
      readonly,
      useQrScanner,
      cssPrefix,
      status,
    } = this.props;

    const depositSelectFieldOptions = depositList.map(deposit => ({
      value: deposit.id,
      label: deposit.name,
    }));

    const loading = status === statuses.LOADING;
    const disabled = readonly || loading;

    return (
      <div className={cssPrefix}>
        {
          useQrScanner && !qrCodeScanned && (
            <QrScanner
              onScanSuccess={this.qrScanSuccessHandler}
              onScanError={this.qrScanErrorHandler}
            />
          )
        }
        {
          [TRANSACTION_TYPES.EXPENSE, TRANSACTION_TYPES.TRANSFER].includes(transactionTypeId) && (
            <SelectField
              value={fromDepositId}
              className={`${cssPrefix}-field`}
              options={depositSelectFieldOptions}
              label="Source Deposit"
              onChange={this.setSelectParam('fromDepositId')}
              error={validationErrors.fromDepositId}
              disabled={disabled}
            />
          )
        }
        {
          [TRANSACTION_TYPES.INCOME, TRANSACTION_TYPES.TRANSFER].includes(transactionTypeId) && (
            <SelectField
              value={toDepositId}
              className={`${cssPrefix}-field`}
              options={depositSelectFieldOptions}
              label="Target Deposit"
              onChange={this.setSelectParam('toDepositId')}
              error={validationErrors.toDepositId}
              disabled={disabled}
            />
          )
        }
        {
          [TRANSACTION_TYPES.EXPENSE, TRANSACTION_TYPES.INCOME].includes(transactionTypeId) && (
            <SelectField
              value={categoryId}
              className={`${cssPrefix}-field`}
              options={categoryList.map(category => ({
                value: category.id,
                label: category.name,
              }))}
              label="Category"
              onChange={this.setSelectParam('categoryId')}
              error={validationErrors.categoryId}
              disabled={disabled}
            />
          )
        }
        {
          [TRANSACTION_TYPES.EXPENSE, TRANSACTION_TYPES.TRANSFER].includes(transactionTypeId) && (
            <InputField
              name="valueFrom"
              value={valueFrom}
              onChange={this.setParam}
              type="number"
              label="Source Value"
              className={`${cssPrefix}-field`}
              error={validationErrors.valueFrom}
              disabled={disabled}
            />
          )
        }
        {
          [TRANSACTION_TYPES.INCOME, TRANSACTION_TYPES.TRANSFER].includes(transactionTypeId) && (
            <InputField
              name="valueTo"
              value={valueTo}
              onChange={this.setParam}
              type="number"
              label="Target Value"
              className={`${cssPrefix}-field`}
              error={validationErrors.valueTo}
              disabled={disabled}
            />
          )
        }
        <InputField
          name="comment"
          value={comment}
          onChange={this.setParam}
          label="Comment"
          className={`${cssPrefix}-field`}
          error={validationErrors.comment}
          disabled={disabled}
        />
        <InputField
          name="transactionDate"
          type="date"
          value={transactionDate}
          onChange={this.setParam}
          label="Date"
          className={`${cssPrefix}-field`}
          error={validationErrors.transactionDate}
          disabled={disabled}
        />
        {
          !readonly && (
            <button
              className={classnames(
                `${cssPrefix}-saveButton`,
                loading && 'button--loading',
              )}
              onClick={this.save}
              disabled={disabled}
            >Save</button>
          )
        }
      </div>
    )
  }
}

TransactionItem.propTypes = {
  transactionTypeId: PropTypes.number,
  depositList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
  })).isRequired,
  categoryList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
  })).isRequired,
  saveHandler: PropTypes.func.isRequired,
  data: PropTypes.shape({}),
  readonly: PropTypes.bool,
  useQrScanner: PropTypes.bool,
  formShouldReset: PropTypes.bool,
  status: PropTypes.shape({}).isRequired,
};

TransactionItem.defaultProps = {
  transactionTypeId: null,
  readonly: false,
  useQrScanner: false,
  data: null,
  formShouldReset: false,
};

export default withCssPrefix(TransactionItem);
