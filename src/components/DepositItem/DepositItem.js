import React from 'react';
import PropTypes from 'prop-types';
import InputField from '../../basicComponents/InputField/InputField';
import SelectField from '../../basicComponents/SelectField/SelectField';
import withCssPrefix from '../../hocs/withCssPrefix';
import './styles.sass';


class DepositItem extends React.PureComponent {
  state = {
    currencyId: null,
    depositTypeId: null,
    name: '',
    balance: 0,
    validationErrors: {
      currencyId: '',
      depositTypeId: '',
      name: '',
      balance: '',
    }
  };

  componentDidMount() {
    const { data } = this.props;
    if (data.id) {
      this.setInitialData(data);
    }
  }

  UNSAFE_componentWillUpdate(nextProps, nextState, nextContext) {
    if (nextProps.data.id && !this.props.data.id) {
      this.setInitialData(nextProps.data);
    }
  }

  setInitialData = (data) => {
    this.setState({
      currencyId: data.currencyId,
      depositTypeId: data.depositTypeId,
      name: data.name,
      balance: parseFloat(data.balance),
    });
  };

  setParam = ({ target: { name, value } }) => {
    this.setState({
      [name]: value,
    })
  };

  setSelectParam = (fieldName) => (option) => {
    this.setState({
      [fieldName]: option.value,
    })
  };

  validateForm = () => {
    const {
      currencyId,
      depositTypeId,
      name,
    } = this.state;
    const validationErrors = {};
    if (!currencyId) {
      validationErrors.currencyId = 'Currency cannot be empty'
    }
    if (!depositTypeId) {
      validationErrors.depositTypeId = 'Type cannot be empty'
    }
    if (!name.trim()) {
      validationErrors.name = 'Name cannot be empty'
    }
    this.setState({
      validationErrors,
    });
    return Object.keys(validationErrors).length === 0;
  };

  saveDeposit = () => {
    const {
      currencyId,
      depositTypeId,
      name,
      balance = 0,
    } = this.state;
    if (this.validateForm()) {
      this.props.saveHandler({
        id: this.props.data.id,
        currencyId,
        depositTypeId,
        name,
        balance,
      });
    }
  };

  render() {
    const {
      currencyId,
      depositTypeId,
      name,
      balance,
      validationErrors,
    } = this.state;
    const {
      depositTypes,
      currencies,
      cssPrefix,
    } = this.props;
    return (
      <div className={cssPrefix}>
        <SelectField
          value={depositTypeId}
          className={`${cssPrefix}-field`}
          options={depositTypes.map(depositType => ({
            value: depositType.id,
            label: depositType.name,
          }))}
          label="Deposit type"
          onChange={this.setSelectParam('depositTypeId')}
          error={validationErrors.depositTypeId}
        />
        <SelectField
          value={currencyId}
          className={`${cssPrefix}-field`}
          options={currencies.map(currency => ({
            value: currency.id,
            label: currency.name,
          }))}
          label="Currency"
          onChange={this.setSelectParam('currencyId')}
          error={validationErrors.currencyId}
        />
        <InputField
          name="name"
          value={name}
          onChange={this.setParam}
          type="text"
          label="Name"
          className={`${cssPrefix}-field`}
          error={validationErrors.name}
        />
        <InputField
          name="balance"
          value={balance}
          onChange={this.setParam}
          label="Balance"
          type="number"
          className={`${cssPrefix}-field`}
          error={validationErrors.balance}
        />
        <button
          className={`${cssPrefix}-saveButton`}
          onClick={this.saveDeposit}
        >Save</button>
      </div>
    )
  }
}

DepositItem.propTypes = {
  depositTypes: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
  })).isRequired,
  currencies: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
  })).isRequired,
};

export default withCssPrefix(DepositItem);
