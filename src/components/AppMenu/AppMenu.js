import React  from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import classnames from 'classnames';

import './appMenu.sass';
import withCssPrefix from "../../hocs/withCssPrefix";


class AppMenu extends React.PureComponent {
  render() {
    const {
      logoutHandler,
      closeHandler,
      cssPrefix,
    } = this.props;

    const pageElements = [
      {
        name: 'Dashboard',
        url: '/dashboard',
        iconName: 'dashboard',
        disabled: false,
      },
      {
        name: 'Transactions',
        url: '/transactions',
        iconName: 'swap_horiz',
        disabled: false,
      },
      {
        name: 'Deposits',
        url: '/deposits',
        iconName: 'account_balance',
        disabled: false,
      },
      {
        name: 'Reports',
        url: '/reports',
        iconName: 'pie_chart',
        disabled: false,
      },
      {
        name: 'Categories',
        url: '/categories',
        iconName: 'category',
        disabled: true,
      },
      {
        name: 'Tags',
        url: '/tags',
        iconName: 'label',
        disabled: true,
      },
    ];
    const settingElements = [
      {
        name: 'Settings',
        url: '/settings',
        iconName: 'settings',
        disabled: true,
      },
      {
        name: 'Sign out',
        url: '/login',
        iconName: 'logout',
        onClick: logoutHandler,
        disabled: false,
      },
    ];

    return (
      <>
        <div className={cssPrefix}>
          <div className={`${cssPrefix}-profileBlock`} />
          <ul className={`${cssPrefix}-pageBlock`}>
            {pageElements.map(pageElement => (
              <li
                key={pageElement.url}
                className={classnames(
                  `${cssPrefix}-pageBlockElement`,
                  pageElement.disabled && `${cssPrefix}-pageBlockElement--disabled`,
                )}
              >
                <Link to={!pageElement.disabled ? pageElement.url : '#'}>
                  <i className={`material-icons ${cssPrefix}-pageIcon`}>{pageElement.iconName}</i>
                  <span className={`${cssPrefix}-pageName`}>{pageElement.name}</span>
                </Link>
              </li>
            ))}
          </ul>
          <ul className={`${cssPrefix}-settingBlock`}>
            {settingElements.map(settingElement => (
              <li className={classnames(
                `${cssPrefix}-pageBlockElement`,
                settingElement.disabled && `${cssPrefix}-pageBlockElement--disabled`,
              )}
              >
                <Link
                  to={!settingElement.disabled ? settingElement.url : '#'}
                  onClick={settingElement.onClick}
                >
                  <i className={`material-icons ${cssPrefix}-pageIcon`}>{settingElement.iconName}</i>
                  <span className={`${cssPrefix}-pageName`}>{settingElement.name}</span>
                </Link>
              </li>
            ))}
          </ul>
        </div>
        <div
          className={`${cssPrefix}-overlay`}
          onClick={closeHandler}
        />
      </>
    );
  }
}

AppMenu.propTypes = {
  logoutHandler: PropTypes.func.isRequired,
  closeHandler: PropTypes.func.isRequired,
};

export default withCssPrefix(AppMenu);
