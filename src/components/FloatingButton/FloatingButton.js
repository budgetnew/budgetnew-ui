import React from 'react';
import classnames from 'classnames';
import withCssPrefix from '../../hocs/withCssPrefix';
import './floatingButton.sass';


export default withCssPrefix(({ onClick, icon, className, cssPrefix }) => {
  let iconElement = icon;
  if (typeof icon === 'string') {
    iconElement = <i className="material-icons">{icon}</i>;
  }

  return (
    <div
      className={classnames(cssPrefix, className)}
      onClick={onClick}
    >
      {iconElement}
    </div>
  );
}, 'floatingButton');
