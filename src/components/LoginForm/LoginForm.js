import React, { PureComponent } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import InputField from '../../basicComponents/InputField/InputField';
import withCssPrefix from '../../hocs/withCssPrefix';
import './LoginForm.sass';
import { POST } from '../../utils/serviceHelper';
import config from '../../../config';


class LoginForm extends PureComponent {
  state = {
    email: '',
    password: '',
    validationErrors: {
      email: '',
      password: '',
    }
  };

  setParam = ({ target: { name, value } }) => {
    this.setState({
      [name]: value,
    })
  };

  validateForm = () => {
    const {
      email,
      password,
    } = this.state;
    const validationErrors = {};
    if (!email.trim()) {
      validationErrors.email = 'Email cannot be empty'
    }
    if (!password.trim()) {
      validationErrors.password = 'Password cannot be empty'
    }
    this.setState({
      validationErrors,
    });
    return Object.keys(validationErrors).length === 0;
  };

  onInputKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.login();
    }
  };

  login = () => {
    if (this.validateForm()) {
      const { email, password } = this.state;
      this.props.loginHandler({ email, password });
    }
  };

  loginViaGoogle = async () => {
    if (!gapi?.auth2?.init) {
      console.log('Google api not loaded!!!');
      return false;
    }
    let authObject = await gapi.auth2.init({
      client_id: config.googleApiClientId,
    });


    if (!authObject.isSignedIn.get()) {
      await authObject.signIn();
    }
    authObject = gapi.auth2.getAuthInstance();
    const currentUser = authObject.currentUser.get();
    const idToken = currentUser.getAuthResponse().id_token;
    try {
      await POST({
        url: '/authViaGoogle',
        data: {
          token: idToken,
        }
      });
      this.props.history.push(`/dashboard`)
    } catch(e) {
      console.error(e);
    }
  };

  render() {
    const {
      email,
      password,
      validationErrors,
    } = this.state;

    const { cssPrefix } = this.props;

    return (
      <div className={cssPrefix}>
        <div className={`${cssPrefix}-inner`}>
          <div className={`${cssPrefix}-title`}>Sign in</div>
          <div>
            <InputField
              name="email"
              value={email}
              onChange={this.setParam}
              onKeyPress={this.onInputKeyPress}
              type="email"
              label="Email"
              className={`${cssPrefix}-inputField`}
              error={validationErrors.email}
            />
            <InputField
              name="password"
              value={password}
              onChange={this.setParam}
              onKeyPress={this.onInputKeyPress}
              label="Password"
              type="password"
              className={`${cssPrefix}-inputField`}
              error={validationErrors.password}
            />
            <button
              className={`${cssPrefix}-submitButton`}
              onClick={this.login}
            >Login</button>
          </div>
          <div className={`${cssPrefix}-alternatives`}>
            <div className={`${cssPrefix}-signInGoogle`}>Sign in via
              <span
                className={`${cssPrefix}-googleIcon`}
                onClick={this.loginViaGoogle}
              />
            </div>
            <div>Register</div>
          </div>
        </div>

      </div>
    );
  }
}

export default compose(
  withRouter,
  withCssPrefix,
)(LoginForm);
