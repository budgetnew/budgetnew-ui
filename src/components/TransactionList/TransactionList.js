import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment/moment';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';
import { compose } from 'recompose';
import QrScannerIcon from './QrScannerIcon';
import FloatingButton from '../FloatingButton/FloatingButton';
import { TRANSACTION_TYPES } from '../../dictionaries/transactionTypes';
import withCssPrefix from '../../hocs/withCssPrefix';
import './transactionList.sass';


class TransactionList extends React.PureComponent {

  state = {
    typeSelected: '',
    depositSelected: null,
    categorySelected: null,
    dateFrom: null,
    dateTo: null,
    dialogTransaction: null,
  };

  renderTransactionIcon = (transaction) => {
    let firstChar = transaction.category?.name ? transaction.category?.name[0] : '';
    return (
      <i>{firstChar}</i>
    );
  };

  navigateToTransactionPageFunc = (query) => () => {
    this.props.history.push(`/transactions/${query}`)
  };

  renderTransactionValue = (transaction) => {
    const { cssPrefix } = this.props;
    const { transactionTypeId } = transaction;
    let transactionValuePrefix = '';
    let currencySign = '';

    if (transactionTypeId === TRANSACTION_TYPES.EXPENSE) {
      transactionValuePrefix = `-`;
      currencySign = transaction.fromDeposit.currency.symbol;
    }
    if (transactionTypeId === TRANSACTION_TYPES.INCOME) {
      transactionValuePrefix = `+`;
      currencySign = transaction.toDeposit.currency.symbol;

    }
    const transactionTypeName = ['expense', 'income', 'transfer'][transactionTypeId - 1];

    return (
      <div className={classnames(
        `${cssPrefix}-itemValue`,
        transactionTypeName && `${cssPrefix}-itemValue--${transactionTypeName}`
      )}>
        {
          !!currencySign && (
            <div
              className={`${cssPrefix}-itemValueCurrency`}
              dangerouslySetInnerHTML={{ __html: currencySign }}
            />
          )
        }
        {
          !!transactionValuePrefix && (
            <div className={`${cssPrefix}-itemValuePrefix`}>{transactionValuePrefix}</div>
          )
        }
        {transactionTypeId === TRANSACTION_TYPES.INCOME ? transaction.valueTo : transaction.valueFrom}
      </div>
    );
  };

  getGroupedTransactions = () => {
    const {
      transactions,
    } = this.props;
    const res = {};
    transactions.forEach((tr) => {
      const timestamp = moment(tr.transactionDate).unix();
      if (!res[timestamp]) {
        res[timestamp] = {
          dateStr: moment(tr.transactionDate).format('Do MMM YYYY'),
          transactions: [],
        };
      }
      res[timestamp].transactions.push(tr);
    });
    return Object.values(res).reverse();
  };

  render() {
    const {
      cssPrefix,
    } = this.props;

    const transactionsGrouped = this.getGroupedTransactions();

    return (
      <div className={cssPrefix}>
        {
          transactionsGrouped.length > 0 && transactionsGrouped.map(({ transactions, dateStr }, idx) => (
            <div
              key={`${dateStr}_${idx}`}
              className={`${cssPrefix}-itemGroup`}
            >
              {transactions.length > 0 && transactions.map(transaction => (
                <div
                  key={transaction.id}
                  className={`${cssPrefix}-item`}
                  onClick={this.navigateToTransactionPageFunc(transaction.id)}
                >
                  {/*<div className={`${cssPrefix}-itemIconWrapper`}>*/}
                  {/*  {this.renderTransactionIcon(transaction)}*/}
                  {/*</div>*/}
                  <div className={`${cssPrefix}-itemInfo`}>
                    <div className={`${cssPrefix}-itemInfoTop`}>
                      <div className={`${cssPrefix}-itemDate`}>
                        {dateStr}
                      </div>
                      {
                        !!transaction.category && (
                          <div className={`${cssPrefix}-itemCategory`}>
                            {transaction.category.name}
                          </div>
                        )
                      }
                    </div>
                    <div className={`${cssPrefix}-itemComment`}>
                      {transaction.comment}
                    </div>
                  </div>
                  {this.renderTransactionValue(transaction)}
                </div>
              ))}
            </div>
          ))
        }
        <FloatingButton
          className={`${cssPrefix}-addButton`}
          onClick={this.navigateToTransactionPageFunc('new')}
          icon="add"
        />
        <FloatingButton
          className={`${cssPrefix}-scanButton`}
          onClick={this.navigateToTransactionPageFunc('new/?scan=1')}
          icon={(
            <i><QrScannerIcon /></i>
          )}
        />
      </div>
    );
  }
}

TransactionList.propTypes = {
  transactions: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    comment: PropTypes.string,
    transactionDate: PropTypes.string,
    transactionTypeId: PropTypes.number,
    valueTo: PropTypes.string,
    valueFrom: PropTypes.string,
    fromDeposit: PropTypes.shape(),
    toDeposit: PropTypes.shape(),
    category: PropTypes.shape(),
  })).isRequired,
};

export default compose(
  withRouter,
  withCssPrefix,
)(TransactionList);
