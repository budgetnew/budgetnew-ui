import React from 'react';


export default function () {
  return (
    <svg width="30" height="25" viewBox="0 0 30 25" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect x="1" y="1" width="28" height="23" rx="2" fill="url(#paint0_linear)" stroke="#DEE1ED" strokeWidth="2" />
      <defs>
        <linearGradient id="paint0_linear" x1="15" y1="0" x2="15" y2="25" gradientUnits="userSpaceOnUse">
          <stop offset="0.395833" stopColor="white" stopOpacity="0" />
          <stop offset="0.395933" stopColor="#DEE1ED" stopOpacity="0.8" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
      </defs>
    </svg>
  );
}
