import React from 'react';
import pcg from '../../../package';

import './productVerision.sass';
import withCssPrefix from '../../hocs/withCssPrefix';


class ProductVersion extends React.PureComponent {
  render() {
    const {
      cssPrefix,
    } = this.props;

    return (
      <div className={cssPrefix}>
        {`v${pcg.version}`}
      </div>
    );
  }
}

export default withCssPrefix(ProductVersion);
