import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import withCssPrefix from '../../hocs/withCssPrefix';
import './transactionTypeTabs.sass';


class TransactionTypeTabs extends React.PureComponent {
  selectTabHandler = (id) => () => {
    const {
      disabled,
    } = this.props;
    !disabled && this.props.onSelect(id);
  };

  render() {
    const {
      transactionTypeList,
      selectedId,
      disabled,
      cssPrefix,
    } = this.props;

    return (
      <div className={classnames(cssPrefix, disabled && `${cssPrefix}--disabled`)}>
        {transactionTypeList.map(transactionType => (
          <div
            key={transactionType.id}
            onClick={this.selectTabHandler(transactionType.id)}
            className={classnames(
              `${cssPrefix}-tab`,
              `${cssPrefix}-tab--${transactionType.name}`,
              selectedId === transactionType.id && `${cssPrefix}-tab--selected`,
              disabled && `${cssPrefix}-tab--disabled`
            )}
          >{transactionType.name}</div>
        ))}
      </div>
    );
  }
}

TransactionTypeTabs.propTypes = {
  transactionTypeList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
  })).isRequired,
  selectedId: PropTypes.number,
  disabled: PropTypes.bool,
};

TransactionTypeTabs.defaultProps = {
  disabled: false,
  selectedId: null,
};

export default withCssPrefix(TransactionTypeTabs);
