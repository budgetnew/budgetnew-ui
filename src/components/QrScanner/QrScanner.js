import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import QrReader from 'react-qr-reader';
import withCssPrefix from '../../hocs/withCssPrefix';
import './QrScanner.sass';


class QrScanner extends PureComponent {
  handleScan = (data) => {
    !!data && this.props.onScanSuccess(data);
  };

  render() {
    const {
      onScanError,
      cssPrefix,
    } = this.props;

    return (
      <div
        className={`${cssPrefix}-wrapper`}
      >
        <QrReader
          delay={300}
          onError={onScanError}
          onScan={this.handleScan}
          className={cssPrefix}
        />
      </div>
    );
  }
}

QrScanner.propTypes = {
  onScanSuccess: PropTypes.func.isRequired,
  onScanError: PropTypes.func.isRequired,
};

export default withCssPrefix(QrScanner);
