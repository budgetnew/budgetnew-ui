import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import withCssPrefix from '../../hocs/withCssPrefix';
import TransactionChartAndData from '../TransactionChartReport/TransactionChartAndData';
import './styles.sass';


class DashboardExpenses extends PureComponent {
  state = {
    selectedCategoryId: null,
  };

  categoryClickHandler = (categoryId) => {
    const {
      loadCategoryExpensesIfAbsent,
    } = this.props;
    loadCategoryExpensesIfAbsent(categoryId);
    this.setState({
      selectedCategoryId: categoryId,
    });
  };

  render() {
    const {
      expenses,
      cssPrefix,
      categoryTransactions,
    } = this.props;

    const { selectedCategoryId } = this.state;

    return (
      <div className={cssPrefix}>
        <TransactionChartAndData
          data={expenses}
          categoryClickHandler={this.categoryClickHandler}
          selectedCategoryId={selectedCategoryId}
          categoryTransactions={categoryTransactions}
        />
      </div>
    );
  }
}

DashboardExpenses.propTypes = {
  expenses: PropTypes.arrayOf(PropTypes.shape({
    sumFrom: PropTypes.string,
    category: PropTypes.shape({
      name: PropTypes.string,
    }),
  })).isRequired,
  loadCategoryExpensesIfAbsent: PropTypes.func.isRequired,
  categoryTransactions: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};

export default withCssPrefix(DashboardExpenses);
