import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import classnames from 'classnames';
import withCssPrefix from '../../hocs/withCssPrefix';
import './appBar.sass';


class AppBar extends PureComponent {
  render() {
    const {
      toggleAppMenu,
      title,
      toggleTransactionFilters,
      transactionFiltersOpened,
      cssPrefix,
    } = this.props;
    return (
      <div className={cssPrefix}>
        <div className={`${cssPrefix}-inner`}>
          <i
            className={`material-icons ${cssPrefix}-mainIcon`}
            onClick={toggleAppMenu}
          >menu</i>
          <div className={`${cssPrefix}-title`}>{title}</div>
          <div className={`${cssPrefix}-rightIconBlock`}>
            <Route
              exact
              path="/transactions"
              render={props => (
                <i
                  className={classnames(
                    'material-icons',
                    `${cssPrefix}-filterIcon`,
                    transactionFiltersOpened && `${cssPrefix}-filterIcon--active`,
                  )}
                  onClick={toggleTransactionFilters}
                >filter_list</i>
              )}
            />
          </div>
        </div>
      </div>
    );
  }
}

AppBar.propTypes = {
  toggleAppMenu: PropTypes.func.isRequired,
  title: PropTypes.string,
  toggleTransactionFilters: PropTypes.func.isRequired,
  transactionFiltersOpened: PropTypes.bool,
};

AppBar.defaultProps = {
  title: '',
  transactionFiltersOpened: true,
};

export default withCssPrefix(AppBar);
