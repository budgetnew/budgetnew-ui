import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import InputField from '../../basicComponents/InputField/InputField';
import { TRANSACTION_TYPES } from '../../dictionaries/transactionTypes';
import withCssPrefix from '../../hocs/withCssPrefix';
import TransactionChartReport from '../TransactionChartReport/TransactionChartAndData';
import './TransactionReports.sass';
import {
  convertToTransactionDate,
  getTransactionDateEndOfMonth,
  getTransactionDateStartOfMonth,
} from '../../utils/date.utils';


class TransactionReports extends PureComponent {
  state = {
    fromDate: null,
    toDate: null,
    selectedCategoryId: null,
    validationErrors: {
      fromDate: '',
      toDate: '',
    }
  };

  componentDidMount() {
    this.setState({
      fromDate: getTransactionDateStartOfMonth(),
      toDate: getTransactionDateEndOfMonth(),
    });
  }

  setParam = (fieldName) => (e) => {
    this.setState({
      [fieldName]: e.target.value,
    })
  };

  validateForm = () => {
    const {
      fromDate,
      toDate,
    } = this.state;
    const validationErrors = {};
    if (!fromDate) {
      validationErrors.fromDate = 'Start Date cannot be empty'
    }
    if (!toDate) {
      validationErrors.toDate = 'End Date cannot be empty'
    }
    this.setState({
      validationErrors,
    });
    return Object.keys(validationErrors).length === 0;
  };

  getReportData = () => {
    if (this.validateForm()) {
      const {
        fromDate,
        toDate,
      } = this.state;
      const { getReportData } = this.props;
      getReportData({
        fromDate: convertToTransactionDate(fromDate),
        toDate: convertToTransactionDate(toDate),
        transactionTypeId: TRANSACTION_TYPES.EXPENSE,
        groupBy: 'category',
      });
    }
  };

  categoryClickHandler = (categoryId) => {
    const {
      categoryTransactions,
      getReportTransactionsForCategory,
    } = this.props;
    const {
      fromDate,
      toDate,
    } = this.state;
    if (!categoryTransactions[categoryId]) {
      getReportTransactionsForCategory({
        fromDate: convertToTransactionDate(fromDate),
        toDate: convertToTransactionDate(toDate),
        categoryId,
        transactionTypeId: TRANSACTION_TYPES.EXPENSE,
      });
    }
    this.setState({
      selectedCategoryId: categoryId,
    });
  };

  renderChart = () => {
    const {
      reportData,
      clearReportData,
      categoryTransactions,
      cssPrefix,
    } = this.props;
    const {
      selectedCategoryId
    } = this.state;

    return (
      <div className={`${cssPrefix}-chartWrapper`}>
        <i
          className={`material-icons ${cssPrefix}-closeButton`}
          onClick={clearReportData}
        >close</i>
        <TransactionChartReport
          data={reportData}
          categoryTransactions={categoryTransactions}
          categoryClickHandler={this.categoryClickHandler}
          selectedCategoryId={selectedCategoryId}
        />
      </div>
    )
  };

  render() {
    const {
      fromDate,
      toDate,
      validationErrors,
    } = this.state;

    const {
      reportData,
      cssPrefix,
    } = this.props;

    const showForm = reportData.length === 0;

    return (
      <div className={cssPrefix}>
        {
          showForm
            ? (
              <div>
                <InputField
                  type="date"
                  value={fromDate}
                  onChange={this.setParam('fromDate')}
                  label="Start Date"
                  className={`${cssPrefix}-field`}
                  error={validationErrors.fromDate}
                />
                <InputField
                  type="date"
                  value={toDate}
                  onChange={this.setParam('toDate')}
                  label="Date to"
                  className={`${cssPrefix}-field`}
                  error={validationErrors.toDate}
                />
                <button
                  className={`${cssPrefix}-sendButton`}
                  onClick={this.getReportData}
                >GET REPORT</button>
              </div>
            )
            : this.renderChart()
        }
      </div>
    );
  }
}

TransactionReports.propTypes = {
  reportData: PropTypes.arrayOf(PropTypes.shape({
    sumFrom: PropTypes.string,
    category: PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string,
    }),
  })).isRequired,
  categoryTransactions: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    comment: PropTypes.string,
    valueFrom: PropTypes.string,
  })).isRequired,
  getReportData: PropTypes.func.isRequired,
  clearReportData: PropTypes.func.isRequired,
  getReportTransactionsForCategory: PropTypes.func.isRequired,
};

export default withCssPrefix(TransactionReports);
