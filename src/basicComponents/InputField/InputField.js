import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './styles.sass';
import withCssPrefix from '../../hocs/withCssPrefix';


class InputField extends React.PureComponent {
  render() {
    const {
      label,
      className,
      error,
      cssPrefix,
      ...otherProps
    } = this.props;
    return (
      <div className={classnames(cssPrefix, className)}>
        {
          label && (
            <label className={`${cssPrefix}-label`}>{label}</label>
          )
        }
        <input
          className={`${cssPrefix}-input ${cssPrefix}-input--date`}
          {...otherProps}
        />
        {
          error && (
            <div className={`${cssPrefix}-error`}>{error}</div>
          )
        }
      </div>

    );
  }
}

InputField.propTypes = {
  type: PropTypes.string,
  value: PropTypes.string.isRequired,
  label: PropTypes.string,
  onChange: PropTypes.func,
  onKeyPress: PropTypes.func,
  className: PropTypes.string,
  error: PropTypes.string,
};

InputField.defaultProps = {
  type: 'text',
  label: null,
  onChange: () => null,
  onKeyPress: () => null,
  className: null,
  error: null,
};

export default withCssPrefix(InputField);
