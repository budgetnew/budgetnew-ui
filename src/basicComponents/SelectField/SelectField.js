import React from 'react';
import Select from 'react-select';
import classnames from 'classnames';
import './styles.sass';
import PropTypes from 'prop-types';
import withCssPrefix from '../../hocs/withCssPrefix';


class SelectField extends React.PureComponent {
  render() {
    const {
      value,
      options,
      label,
      className,
      onChange,
      error,
      disabled,
      isSearchable,
      cssPrefix,
    } = this.props;
    const selectedValue = value === null ? null : options.find(option => option.value === value);
    return (
      <div className={classnames(cssPrefix, className)}>
        <label className={`${cssPrefix}-label`}>{label}</label>
        <Select
          className={`${cssPrefix}-select`}
          classNamePrefix={cssPrefix}
          value={selectedValue}
          options={options}
          onChange={onChange}
          isDisabled={disabled}
          isSearchable={isSearchable}
        />
        {
          error && (
            <div className={`${cssPrefix}-error`}>{error}</div>
          )
        }
      </div>
    );
  }
}

SelectField.propTypes = {
  value: PropTypes.oneOf(
    PropTypes.string,
    PropTypes.number,
  ),
  options: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  label: PropTypes.string,
  className: PropTypes.string,
  onChange: PropTypes.func,
  isSearchable: PropTypes.bool,
};

SelectField.defaultProps = {
  label: null,
  className: null,
  value: null,
  onChange: () => null,
  isSearchable: false,
};

export default withCssPrefix(SelectField);
