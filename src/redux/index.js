import { combineReducers } from 'redux';
import auth from './auth';
import dashboard from './dashboard';
import deposits from './deposits';
import transactions from './transactions';
import transactionTypes from './transactionTypes';
import categories from './categories';
import depositTypes from './depositTypes';
import currencies from './currencies';
import reports from './reports';
import tags from './tags';
import appBar from './appBar';

import snackMessage from './snackMessage';
import layout from './layout';


const rootReducer = combineReducers({
  auth,
  dashboard,
  deposits,
  transactions,
  transactionTypes,
  categories,
  depositTypes,
  currencies,
  reports,
  tags,
  appBar,

  snackMessage,
  layout,
});

export default rootReducer;
