import { SUCCESS } from '../dictionaries/actionPostfix';


export const GET_CURRENCIES = 'GET_CURRENCIES';

const initialState = [];

export default (currencies = initialState, action) => {
  switch (action.type) {
    case GET_CURRENCIES + SUCCESS:
      return action.data;
    default:
      return currencies;
  }
};


export const getCurrenciesSagaAC = () => ({ type: GET_CURRENCIES });
