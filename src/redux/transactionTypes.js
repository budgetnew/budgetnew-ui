import { SUCCESS } from '../dictionaries/actionPostfix';


export const LOAD_TRANSACTION_TYPE_LIST = 'LOAD_TRANSACTION_TYPE_LIST';

const initialState = [];

export default (transactionTypes = initialState, action) => {
  switch (action.type) {
    case LOAD_TRANSACTION_TYPE_LIST + SUCCESS:
      return action.data;
    default:
      return transactionTypes;
  }
};


export const loadTransactionTypesSagaAC = () => ({
  type: LOAD_TRANSACTION_TYPE_LIST,
});
