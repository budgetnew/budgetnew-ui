import { SUCCESS } from '../dictionaries/actionPostfix';


export const GET_TAGS = 'GET_TAGS';

const initialState = [];

export default (tags = initialState, action) => {
  switch (action.type) {
    case GET_TAGS + SUCCESS:
      return action.data;
    default:
      return tags;
  }
};


export const getTagsSagaAC = () => ({ type: GET_TAGS });
