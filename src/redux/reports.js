import { produce } from 'immer';
import { SUCCESS } from '../dictionaries/actionPostfix';


export const GET_REPORT_DATA = 'GET_REPORT_DATA';
export const GET_REPORT_TRANSACTIONS_FOR_CATEGORY = 'GET_REPORT_TRANSACTIONS_FOR_CATEGORY';
export const CLEAR_REPORT_DATA = 'CLEAR_REPORT_DATA';

const initialState = {
  reportData: [],
  categoryTransactions: {},
};

export default (reports = initialState, action) => (
  produce(reports, (draft) => {
    switch (action.type) {
      case GET_REPORT_DATA + SUCCESS: {
        draft.reportData = action.data;
        break;
      }

      case GET_REPORT_TRANSACTIONS_FOR_CATEGORY + SUCCESS: {
        const { categoryId } = action.serviceArgs.params;
        draft.categoryTransactions[categoryId] = action.data;
        break;
      }

      case CLEAR_REPORT_DATA: {
        return initialState;
      }

      default:
        return reports;
    }
  })
);

export const clearReportDataAC = () => ({ type: CLEAR_REPORT_DATA });

export const getReportDataSagaAC = data => ({ type: GET_REPORT_DATA, data });
export const getReportTransactionsForCategorySagaAC = data => ({
  type: GET_REPORT_TRANSACTIONS_FOR_CATEGORY,
  data,
});
