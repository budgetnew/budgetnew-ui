import { produce } from 'immer';

export const CLOSE_TRANSACTION_FILTERS = 'CLOSE_TRANSACTION_FILTERS';
export const TOGGLE_TRANSACTION_FILTERS = 'TOGGLE_TRANSACTION_FILTERS';

const initialState = {
  transactionFiltersOpened: false,
};

export default (transactions = initialState, action) => (
  produce(transactions, (draft) => {
    switch (action.type) {
      case TOGGLE_TRANSACTION_FILTERS: {
        draft.transactionFiltersOpened = !draft.transactionFiltersOpened;
        break;
      }

      case CLOSE_TRANSACTION_FILTERS: {
        draft.transactionFiltersOpened = false;
        break;
      }

      default:
        return transactions;
    }
  })
);


export const toggleTransactionFiltersAC = () => ({ type: TOGGLE_TRANSACTION_FILTERS });
export const closeTransactionFiltersAC = () => ({ type: CLOSE_TRANSACTION_FILTERS });
