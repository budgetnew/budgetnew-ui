import { SUCCESS } from '../dictionaries/actionPostfix';


export const LOAD_DASHBOARD_EXPENSES = 'LOAD_DASHBOARD_EXPENSES';

const initialState = {
  expenses: [],
};

export default (dashboard = initialState, action) => {
  switch (action.type) {
    case LOAD_DASHBOARD_EXPENSES + SUCCESS: {
      dashboard.expenses = action.data;
      return {
        expenses: action.data,
      };
    }

    default:
      return dashboard;
  }
};


export const loadDashboardExpensesSagaAC = data => ({ type: LOAD_DASHBOARD_EXPENSES, data });
