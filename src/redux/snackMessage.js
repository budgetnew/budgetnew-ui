import { toast } from 'react-toastify';


export const SHOW_SNACK_MESSAGE = 'SHOW_SNACK_MESSAGE';
export const HIDE_SNACK_MESSAGE = 'HIDE_SNACK_MESSAGE';

const autoCloseTime = {
  success: 1500,
  default: 4000,
};

const initialState = {};

export default (snackMessage = initialState, action) => {
  switch (action.type) {
    case SHOW_SNACK_MESSAGE: {
      const {
        data: {
          text,
          type,
        },
      } = action;
      toast(text, {
        type,
        autoClose: autoCloseTime[type] || autoCloseTime.default,
      });
      break;
    }
    default:
      break;
  }

  return snackMessage;
};


export const showSnackMessageSagaAC = (text, type) => ({ type: SHOW_SNACK_MESSAGE, data: { text, type } });
export const hideSnackMessageSagaAC = () => ({ type: HIDE_SNACK_MESSAGE });
