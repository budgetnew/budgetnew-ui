import { SUCCESS } from '../dictionaries/actionPostfix';


export const GET_DEPOSIT_TYPES = 'GET_DEPOSIT_TYPES';

const initialState = [];

export default (depositTypes = initialState, action) => {
  switch (action.type) {
    case GET_DEPOSIT_TYPES + SUCCESS:
      return action.data;
    default:
      return depositTypes;
  }
};


export const getDepositTypesSagaAC = () => ({ type: GET_DEPOSIT_TYPES });
