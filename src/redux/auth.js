import { SUCCESS } from '../dictionaries/actionPostfix';


export const LOG_IN = 'LOG_IN';
export const LOG_OUT = 'LOG_OUT';
export const GET_AUTH_USER = 'GET_AUTH_USER';

const initialState = {};

export default (auth = initialState, action) => {
  switch (action.type) {
    case LOG_IN + SUCCESS: {
      return action.data;
    }

    case LOG_OUT + SUCCESS: {
      return {};
    }

    case GET_AUTH_USER + SUCCESS: {
      return action.data || initialState;
    }

    default:
      return auth;
  }
};


export const loginSagaAC = data => ({ type: LOG_IN, data });
export const logoutSagaAC = () => ({ type: LOG_OUT });
export const getAuthUserSagaAC = () => ({ type: GET_AUTH_USER });
