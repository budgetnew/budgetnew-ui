import { SUCCESS } from '../dictionaries/actionPostfix';


export const LOAD_CATEGORY_LIST = 'LOAD_CATEGORY_LIST';

const initialState = [];

export default (categories = initialState, action) => {
  switch (action.type) {
    case LOAD_CATEGORY_LIST + SUCCESS:
      return action.data.sort((a, b) => {
        if (a.name > b.name) {
          return 1;
        }
        if (a.name < b.name) {
          return -1;
        }
        return 0;
      });
    default:
      return categories;
  }
};


export const loadCategoriesSagaAC = transactionTypeId => ({
  type: LOAD_CATEGORY_LIST,
  payload: { transactionTypeId },
});
