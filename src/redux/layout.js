export const TOGGLE_DRAWER = 'TOGGLE_DRAWER';
export const HIDE_DRAWER = 'HIDE_DRAWER';
export const SHOW_DRAWER = 'SHOW_DRAWER';
export const SET_MOBILE = 'SET_MOBILE';
export const SET_APP_BAR_TITLE = 'SET_APP_BAR_TITLE';

const initialState = {
  drawer: {
    isOpened: true,
  },
  isMobile: false,
  appBarTitle: '',
};

export default (layout = initialState, action) => {
  switch (action.type) {
    case HIDE_DRAWER: {
      const newLayout = Object.assign({}, layout);
      newLayout.drawer.isOpened = false;
      return newLayout;
    }

    case SHOW_DRAWER: {
      const newLayout = Object.assign({}, layout);
      newLayout.drawer.isOpened = true;
      return newLayout;
    }

    case TOGGLE_DRAWER: {
      const newLayout = Object.assign({}, layout);
      newLayout.drawer.isOpened = !layout.drawer.isOpened;
      return newLayout;
    }

    case SET_MOBILE: {
      const newLayout = Object.assign({}, layout);
      newLayout.isMobile = action.data;
      return newLayout;
    }

    case SET_APP_BAR_TITLE: {
      const newLayout = Object.assign({}, layout);
      newLayout.appBarTitle = action.data;
      return newLayout;
    }

    default:
      return layout;
  }
};


export const toggleDrawerAC = () => ({ type: TOGGLE_DRAWER });
export const setMobileAC = isMobile => ({ type: SET_MOBILE, data: isMobile });
export const hideDrawerAC = () => ({ type: HIDE_DRAWER });
export const showDrawerAC = () => ({ type: SHOW_DRAWER });
export const setAppBarTitleAC = title => ({ type: SET_APP_BAR_TITLE, data: title });
