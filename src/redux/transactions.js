import { produce } from 'immer';
import { SUCCESS, START } from '../dictionaries/actionPostfix';
import statuses from '../dictionaries/statuses';

export const GET_TRANSACTIONS = 'GET_TRANSACTIONS';
export const LOAD_TRANSACTION = 'LOAD_TRANSACTION';
export const ADD_TRANSACTION = 'ADD_TRANSACTION';
export const SAVE_TRANSACTION = 'SAVE_TRANSACTION';

const initialState = {
  list: [],
  item: {
    status: '',
    formShouldReset: false,
  },
};

export default (transactions = initialState, { type, data }) => (
  produce(transactions, (draft) => {
    switch (type) {
      case GET_TRANSACTIONS + SUCCESS: {
        draft.list = data;
        break;
      }

      case LOAD_TRANSACTION + SUCCESS: {
      // case SAVE_TRANSACTION + SUCCESS: {
        const transactionIndex = transactions.findIndex(transaction => transaction.id === data.id);
        if (transactionIndex >= 0) {
          draft[transactionIndex] = data;
        }
        draft.list.push(data);
        break;
      }

      case SAVE_TRANSACTION + START: {
        draft.item = {
          formShouldReset: false,
          status: statuses.LOADING,
        };
        break;
      }

      case SAVE_TRANSACTION + SUCCESS: {
        draft.item = {
          formShouldReset: true,
          status: statuses.SUCCESS,
        };
        break;
      }

      default:
        return transactions;
    }
  })
);


export const loadTransactionListSagaAC = data => ({ type: GET_TRANSACTIONS, data });
export const loadTransactionSagaAC = id => ({ type: LOAD_TRANSACTION, payload: { id } });
export const saveTransactionSagaAC = transactionData => ({ type: SAVE_TRANSACTION, data: transactionData });
export const addTransactionSagaAC = transactionData => ({ type: ADD_TRANSACTION, data: transactionData });

export const getTransactionByIdFunc = state => id => state.transactions.list.find(transaction => `${transaction.id}` === id);
