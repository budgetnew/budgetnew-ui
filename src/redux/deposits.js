import { produce } from 'immer';
import { SUCCESS } from '../dictionaries/actionPostfix';


export const GET_DEPOSITS = 'GET_DEPOSITS';
export const GET_DEPOSIT = 'GET_DEPOSIT';
export const SAVE_DEPOSIT = 'SAVE_DEPOSIT';

const initialState = [];

export default (deposits = initialState, action) => (
  produce(deposits, (draft) => {
    switch (action.type) {
      case GET_DEPOSITS + SUCCESS:
        return action.data;

      case GET_DEPOSIT + SUCCESS:
      case SAVE_DEPOSIT + SUCCESS: {
        const depositIndex = deposits.findIndex(deposit => deposit.id === action.data.id);
        if (depositIndex >= 0) {
          draft[depositIndex] = action.data;
        }
        draft.push(action.data);
        break;
      }
    }
  })
);


export const loadDepositListSagaAC = () => ({ type: GET_DEPOSITS });
export const loadDepositSagaAC = id => ({ type: GET_DEPOSIT, payload: { id } });
export const saveDepositSagaAC = data => ({ type: SAVE_DEPOSIT, data });


export const getDepositByIdFunc = state => id => state.deposits.find(deposit => `${deposit.id}` === id);
