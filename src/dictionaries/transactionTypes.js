export const TRANSACTION_TYPES = {
  EXPENSE: 1,
  INCOME: 2,
  TRANSFER: 3,
};
