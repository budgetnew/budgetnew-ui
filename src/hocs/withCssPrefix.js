import React from 'react';
import { getDisplayName } from 'recompose';


const makeFirstLetterLowcase = str => str[0].toLowerCase() + str.slice(1);

export default function withCssPrefix(BaseComponent, className) {
  return (props) => {
    let InitialComponent = BaseComponent;
    while (InitialComponent.WrappedComponent) {
      InitialComponent = InitialComponent.WrappedComponent;
    }
    const cssPrefix = className || makeFirstLetterLowcase(getDisplayName(InitialComponent));

    return (
      <BaseComponent
        {...props}
        cssPrefix={cssPrefix}
      />
    );
  };
}
