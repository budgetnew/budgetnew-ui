import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import 'whatwg-fetch';
import { Provider } from 'react-redux';
import store from './redux/store';
import 'react-toastify/dist/ReactToastify.css';
import './styles/index.sass';

import AppRouter from './routes/AppRouter';
import ProductVersion from './components/ProductVerion/ProductVersion';

// injectTapEventPlugin();
ReactDOM.render(
  <Provider store={store}>
    <AppRouter />
    <ProductVersion />
  </Provider>,
  document.getElementById('root'),
);
