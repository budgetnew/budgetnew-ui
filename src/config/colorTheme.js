import * as _colors from 'material-ui/styles/colors';
import * as _colorManipulator from 'material-ui/utils/colorManipulator';
import _spacing from 'material-ui/styles/spacing';

const _interopRequireDefault = obj => (obj && obj.__esModule ? obj : { default: obj });
const _spacing2 = _interopRequireDefault(_spacing);

export default {
  spacing: _spacing2.default,
  fontFamily: 'Roboto, sans-serif',
  borderRadius: 2,
  palette: {
    primary1Color: _colors.blueGrey500,
    primary2Color: _colors.blueGrey700,
    primary3Color: _colors.grey400,
    accent1Color: _colors.blueA200,
    accent2Color: _colors.blue100,
    accent3Color: _colors.blue500,
    textColor: _colors.grey900,
    secondaryTextColor: (0, _colorManipulator.fade)(_colors.darkBlack, 0.54), // _colors.grey600,
    alternateTextColor: _colors.white,
    canvasColor: _colors.white,
    borderColor: _colors.grey400,
    disabledColor: (0, _colorManipulator.fade)(_colors.darkBlack, 0.3),
    pickerHeaderColor: _colors.blue500,
    clockCircleColor: (0, _colorManipulator.fade)(_colors.darkBlack, 0.07),
    shadowColor: _colors.fullBlack,
  },
};
